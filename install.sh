sudo apt update
wget https://raw.githubusercontent.com/docker/docker-install/master/install.sh -O install_docker.sh
chmod +x install_docker.sh
./install_docker.sh
echo '{"mtu":1450}' | sudo tee /etc/docker/daemon.json
sudo groupadd docker
sudo usermod -aG docker $USER
sudo systemctl restart docker

git clone --branch develop https://github.com/scaleoutsystems/fedn.git
